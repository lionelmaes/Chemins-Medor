<?php

    $parentQuestion = $pages->get('template=question, link_answer='.$page->id);

    if($parentQuestion->watched == 1){

 include(\ProcessWire\wire('files')->compile('inc/data.inc',array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
    }

?>

<?php

    if(empty($page->link_question))
        header('Location: /index.php');

    $question = $page->link_question;
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . 'site/templates/question.php',array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));

?>
</div>
