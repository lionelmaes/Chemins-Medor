<?php

    if(!$config->ajax){
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/inc/header.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
    }

    $article = $page;
?>
<?php

    if($article->image):
?>

<section class="illu">
        <img src="<?php echo $article->image; ?>" />
</section>

<?php
    endif;
?>

    <section class="page">
    <section class="article-full>">
        <div class="article-container">
          <h3><?php echo $article->title; ?></h3>
          <div class="detail"><?php echo $article->text; ?></div>
        </div>
    </section>
</section>


<?php
    if(!$config->ajax){
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/inc/footer.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)));
    }
?>
