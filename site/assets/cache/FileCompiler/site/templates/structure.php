<?php

    function setPageInfo($p, $elem, $page){
            $p->setOutputFormatting(false);

            $p->template = ($elem->type == 0)?'question':'answer';
            $p->parent = ($elem->type == 0)?$page->parent->child('template=questions'):$page->parent->child('template=answers');
            if($elem->type == 0)
                $p->watched = $elem->watched;
            $p->title = $elem->title;
            $p->position = json_encode($elem->position);

            return $p;
    }

    function setPageLinks($p, $elem, $page, $newElems){
        $p->setOutputFormatting(false);


        if($elem->type == 0){
            $p->link_answer->removeAll();
        }else{
            $p->link_question = false;
        }

        foreach($elem->out as $link){
            if($link->id == ''){
                //on va devoir choper l'id de l'élément via son id temporaire
                foreach($newElems as $elem2){
                    if($elem2->inId == $link->inId){
                        $linkId = $elem2->id;
                    }
                }
            }
            else{
                $linkId = $link->id;
            }

            if(!isset($linkId)) return false;

            if($elem->type == 0){
                $p->link_answer->add($linkId);
            }else{
                $p->link_question = $linkId;
            }

       }
       return $p;

    }


    if($config->ajax){


        if(isset($_POST['action'])){
             switch($_POST['action']){
                case 'load':
                    $boxes = array();
                    $edges = array();
                    $elements = $pages->find('template=question|answer');

                    foreach($elements as $element){
                        $boxes[] = array('id'=>$element->id,
                                         'type'=>($element->template == 'question')?0:1,
                                         'name'=>$element->name,
                                         'text'=>html_entity_decode($element->title, ENT_QUOTES|ENT_HTML5),
                                         'adminURL'=>$config->urls->admin.'page/edit/?id='.$element->id,
                                         'publicURL' => $element->url,
                                         'position'=>$element->position,
                                         'textfield'=>($element->textfield == 1)?1:0,
                                         'watched'=>($element->watched == 1)?1:0
                                     );


                        if($element->template == 'question'){
                            foreach($element->link_answer as $link){
                                $edges[] = array($element->id, $link->id);
                            }
                        }else{
                            if($element->link_question)
                               $edges[] = array($element->id, $element->link_question->id);
                        }

                    }

                    echo json_encode(array('status'=>'ok', 'boxes'=>$boxes, 'edges'=>$edges));
                    break;
                case 'save':
                    $data = json_decode($_POST['data']);
                    $newPages = array();
                    //on commence par insérer les nouvelles boites (pour leur donner un id)
                    foreach($data->insert as $elem){

                        $p = new \ProcessWire\Page();
                        $p = setPageInfo($p, $elem, $page);
                        $p->save();
                        $elem->id = $p->id;
                        $elem->url = $config->urls->admin.'page/edit/?id='.$elem->id;
                        $elem->puburl = $p->url;
                        $newPages[] = $elem;


                    }
                    //on fait les liens!
                    foreach($data->insert as $elem){
                        $p = $pages->get($elem->id);
                        $p = setPageLinks($p, $elem, $page, $data->insert);
                        if($p !== false)
                            $p->save();
                    }
                    foreach($data->update as $elem){
                        $p = $pages->get($elem->id);
                        $p = setPageInfo($p, $elem, $page);
                        $p = setPageLinks($p, $elem, $page, $data->insert);
                        $p->save();

                    }

                    foreach($data->delete as $id){
                      $p = $pages->get($id);
                      $p->delete();

                    }

                    echo json_encode(array('status'=>'ok', 'newElems'=>$newPages));
                    break;
                case 'getTrailCount':

                    if(!preg_match('#[0-9|>]+#', $_POST['trail']))
                        echo json_encode(array('status'=>'ko', 'error'=>'trail format error:'.$_POST['trail']));


                    $count = $pages->count('template=session, trail%=\''.$_POST['trail'].'\'');

                    echo json_encode(array('status'=>'ok', 'count'=>$count, 'trail'=>$_POST['trail']));
                    break;
                case 'getBoxFreq':
                    $data = json_decode($_POST['data']);
                    $boxes = array();
                    foreach($data->boxes as $boxId){

                         $count = $pages->count('template=session, trail%=\''.$boxId.'\'');

                         $boxes[] = array('id'=>$boxId, 'freq'=>$count);
                    }
                    echo json_encode(array('status'=>'ok', 'boxes'=>$boxes));
                    break;
                case 'loadAnswer':
                    $data = json_decode($_POST['data']);
                    $answers = array();
                    $sessions = $pages->find('template=session, trail%=\''.$data->trail.'\'');
                    foreach($sessions as $session){
                        foreach($session->textdatas as $textdata){
                            if($textdata->answer->id == $data->id){
                                $answers[] = $textdata->textdata;
                                break;
                            }
                        }
                    }
                    echo json_encode(array('status'=>'ok', 'answers'=>$answers));


            }

        }


    }

    else{
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title><?php echo $page->title; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/normalize.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/fonts.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/structure.css" />
        <script src="<?php echo $config->urls->templates?>scripts/jquery-3.1.1.min.js" type="text/javascript"></script>

	</head>
	<body>
    <header>
       <div class="ui">mode: <a data-function="mode" href="<?php echo $page->httpUrl; ?>">write</a></div><div class="ui"><a href="<?php echo $page->httpUrl; ?>" data-function="save">save</a></div>

    </header>

    <main>
       <section class="structure">
       <a href="<?php echo $page->httpUrl; ?>" class="ui expand" data-mode="0" data-direction="top"></a>
       <a href="<?php echo $page->httpUrl; ?>" class="ui expand" data-mode="0" data-direction="right"></a>
       <a href="<?php echo $page->httpUrl; ?>"  class="ui expand" data-mode="0" data-direction="bottom"></a>
       <a href="<?php echo $page->httpUrl; ?>"  class="ui expand" data-mode="0" data-direction="left"></a>
       </section>
    </main>


	<script src="<?php echo $config->urls->templates?>scripts/structure.js" type="text/javascript"></script>
	</body>
</html>
<?php
}
?>
