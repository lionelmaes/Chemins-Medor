/*GLOBAL VARS*/

var FOCUS = null;
var $CONTAINER = $('main section.structure');
var MODE = 0;
var CTRL = false;


//relations between box types (0=question, 1=answer)
var edgesRules = {
    '0>1':2, //multiple
    '1>0':1, //unique
    '0>0':0, //aucune
    '1>1':0
};


var BOXES = [];
var DATABOXES = [];
var EDGES = [];
var CHANGES = {'insert':[], 'update':[], 'delete':[]};

/*END GLOBAL VARS*/


/*MAIN FUNCTIONS DEFINITIONS*/
function initEvents(){
    if(MODE == 0)
        initEventsWrite();
    else if(MODE == 1)
        initEventsData();
    
}

function initEventsWrite(){
    $(window).on('beforeunload', function(e){
        if(CHANGES.insert.length > 0 || CHANGES.update.length > 0 || CHANGES.delete.length > 0)
            return 'There have been some changes since the last save. Quit anyway?';
    });
    
    $('body').on('keypress', function(e){
       e.preventDefault();
       
       if(e.which !== 0 && e.which !== 8 && FOCUS != null){
           var c = String.fromCharCode(e.which);
           FOCUS.addChar(c); 
           
        }
       
       
    }).on('keydown', function(e){
        
        if(e.keyCode == 17){
            CTRL = true;
            return;
        }
        
        if(FOCUS == null) return;
        
        if(e.keyCode == 8){
            FOCUS.removeChar();
            
        }
        
        
    }).on('keyup', function(e){
        if(e.keyCode == 17){
            
            CTRL = false;
            return;
        }
    
    }).on('mousemove', function(e){
        if(FOCUS != null && FOCUS.draggable){
            FOCUS.setPosition(e.pageX - FOCUS.mouseOffset.x, e.pageY - FOCUS.mouseOffset.y);
        }
    
    });
    
    $('.ui a').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        
        if($(this).data('function') == 'save')
            saveStructure();
        else if($(this).data('function') == 'mode')
            switchMode();
        
    });
        
    $CONTAINER.on('dblclick', function(e){
        removeFocus();
        
        var box = makeBox(e.pageX-$CONTAINER.offset().left, e.pageY-$CONTAINER.offset().top, true);
        CHANGES.insert.push(box);
        box.setFocus();
        
    });
    
    $CONTAINER.find('.ui.expand').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        
        var cW = $CONTAINER.width();
        var cH = $CONTAINER.height();
        
        var direction = $(this).data('direction');
        switch(direction){
            case 'top':
                for(var i in BOXES){
                    BOXES[i].setFocus();
                    BOXES[i].setPosition(BOXES[i].position.x, BOXES[i].position.y + $(window).height()/2);
                }
                $('html, body').scrollTop($(window).height()/2);
                $('html, body').animate({ scrollTop: 0 }, 1000);
                break;
            case 'right':
                $CONTAINER.css('width', cW + $(window).width()/2);
                
                $('html, body').animate({ scrollLeft: $(document).width() }, 1000);
                break;
            case 'bottom':
                
                $CONTAINER.css('height', cH + $(window).height()/2);
                $('html, body').animate({ scrollTop: $(document).height() }, 1000);
                break;
            case 'left':
                for(var i in BOXES){
                    BOXES[i].setFocus();
                    BOXES[i].setPosition(BOXES[i].position.x + $(window).width()/2, BOXES[i].position.y);
                }
                $('html, body').scrollLeft($(window).width()/2);
                $('html, body').animate({ scrollLeft: 0 }, 1000);
                break;
            
        }
        
    
    });
    
}

function initEventsData(){
    $('.ui a').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        
        if($(this).data('function') == 'save'){
            exportStructure(e);
        }else if($(this).data('function') == 'mode')
            switchMode();
        
    });
    $CONTAINER.on('click', function(e){
        for(var i in BOXES){
                BOXES[i].removeFocus();
        }
    });
 
}
function exportStructure(e){
    
    var dataPoints = [];
    for(var i in BOXES){
        if(BOXES[i].dataPoint){
            dataPoints.push(BOXES[i]);
        }
        
    }
    
    if(dataPoints.length == 0){
        alert('Please make a selection (click on box) before export');
        return;
    }
    
    $CONTAINER.hide();
    var $exportZone = $('<section class="export-zone"></section>');
    $CONTAINER.after($exportZone);
    var analyser = new Analyser(dataPoints, $exportZone);
}

function switchMode(){
        $CONTAINER.show();
        $('section.export-zone').remove();
        
        $('*[data-mode='+MODE+']').hide();
        MODE = (MODE == 0)?1:0;
        $('*[data-mode='+MODE+']').show();
        removeEvents();
        
        switch(MODE){
            case 1:
                DATABOXES = [];
                $('.ui a[data-function=mode').text('data');
                $('.ui a[data-function=save').text('export');
                for(var i in BOXES){
                    if(BOXES[i].watched == 0 || BOXES[i].id == null) {
                        BOXES[i].hide();   
                    } else{
                        DATABOXES.push(BOXES[i]);
                        
                    }
                    BOXES[i].removeFocus();
                }
                showBoxFreq();
                break;
            case 0:
                $('.ui a[data-function=mode').text('write');
                $('.ui a[data-function=save').text('save');
                for(var i in BOXES){
                    BOXES[i].show();
                    BOXES[i].removeDataPoint();
                    BOXES[i].removeFocus();
                        
                }
                break;
        }
        
        for(var i in BOXES){
            BOXES[i].initEvents();
        }
        for(var i in EDGES){
            EDGES[i].initEvents();
        }
        
        initEvents();
}
//TODO: montrer les fréquentations boite par boite
function showBoxFreq(){
    
    for(var i in DATABOXES){
        var output = {'boxes':[]}
        output.boxes.push(DATABOXES[i].id);
        
        $.ajax({
            url : $(location).attr('href'),
            type: 'POST',
            data : {'action':'getBoxFreq', 'data':JSON.stringify(output)},
            contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
            dataType:'json',
            success:function(data, textStatus, jqXHR){
                for(var j in data.boxes){
                    for(var k in DATABOXES){
                        if(DATABOXES[k].id == data.boxes[j].id){
                            DATABOXES[k].setFreq(data.boxes[j].freq);
                            break;
                        }
                    }
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(textStatus);
                console.log(errorThrown);
            }
            });
    }
    
        
    //console.log(output);
    
}

function removeEvents(){
    $('body').off();
    $('body').find('*').off();

}

function saveStructure(){

    
    var output = {'insert':[], 'update':[], 'delete':CHANGES.delete};
    for(var i in CHANGES.insert){
        output.insert.push(CHANGES.insert[i].getDBObj());
    }
    for(var i in CHANGES.update){
        output.update.push(CHANGES.update[i].getDBObj());
    }
    $('body').append('<div id="data-process">');
    //console.log(output);
    $.ajax({
        url : $(location).attr('href'),
        type: 'POST',
        data : {'action':'save', 'data':JSON.stringify(output)},
        contentType: 'application/x-www-form-urlencoded;charset=UTF-8',
        dataType:'json',
        success:function(data, textStatus, jqXHR){
            
            for(var i in CHANGES.insert){
                
                for(var j in data.newElems){
                    
                    if(CHANGES.insert[i].inId == data.newElems[j].inId){
                        
                        CHANGES.insert[i].id = data.newElems[j].id;
                        CHANGES.insert[i].setAdminURL(data.newElems[j].url);
                    }
                }
            }
            CHANGES = {'insert':[], 'update':[], 'delete':[]};
            $('#data-process').remove();
            
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
   
   
}

function loadStructure(){
    
    $.ajax({
            url : $(location).attr('href'),
            type: "POST",
            data : {'action':'load'},
           
            dataType:'json',
            success:function(data, textStatus, jqXHR){
                for(var i in data.boxes){
                    loadBox(data.boxes[i]);
                }
                for(var i in data.edges){
                    loadEdges(data.edges[i]);
                }
                
                updateContainerSize();
                updateWatchedStatus();
                
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(errorThrown);
            }
        });
}

function loadBox(data){
    
    if(data.position == ""){
        data.position = {x:Math.random() * $CONTAINER.width(), y:Math.random() * $CONTAINER.height()};
    }else{
        data.position = JSON.parse(data.position);
        
    }
    var box = makeBox(data.position.x, data.position.y);
    box.id = data.id;
    box.setAdminURL(data.adminURL);
    box.setText(data.text);
    box.setType(data.type);
    if(data.type == 0)
        box.setWatched(data.watched);
        
    

}

function loadEdges(data){
    var boxIn = getBoxById(data[1]);
    var boxOut = getBoxById(data[0]);
    makeEdge(boxOut, boxIn);
}

function getBoxById(id){
    for(var i in BOXES){
        if(BOXES[i].id == id)
            return BOXES[i];
    }
    return false;
}

function makeBox(x, y, centered){
    
    
    var box = new Box($CONTAINER);
    $(box).on('focusIn', onBoxFocusIn);
    $(box).on('focusOut', onBoxFocusOut);
    $(box).on('delete', onBoxDelete);
    $(box).on('watch', onBoxWatch);
    $(box).on('dataPoint', onBoxDataPoint);
    $(box).on('drag', onBoxDrag);
    
    if(centered){
        x -= box.$box.outerWidth()/2;
        y -= box.$box.outerHeight()/2;
    }
    
    box.setPosition(x, y);
    BOXES.push(box);
    box.initEvents();
    
    return box;

}

function makeEdge(boxOut, boxIn){
    
    //avoid self edge
    if(boxOut == boxIn)
        return false;
    
    //avoid existing edge
    for(var i in EDGES)
        if(EDGES[i].boxOut == boxOut && EDGES[i].boxIn == boxIn)
            return false;
    
    
    var ruleIndex = boxOut.type +'>'+ boxIn.type;
    
    var rule = edgesRules[ruleIndex];
    
    if(rule == 0)
        return false;
        
    else if(rule == 1){
        if(boxOut.outEdges.length > 0)
            return false;
    }
    
    var edge = new Edge(boxOut, boxIn, $CONTAINER);
    edge.initEvents();
    $(edge).on('delete', onEdgeDelete);      
    boxOut.addOutEdge(edge);
    boxIn.addInEdge(edge);
    
    
    EDGES.push(edge);
    
    return edge;
}

function removeFocus(exceptions){
    if(exceptions == undefined)
        exceptions = [];
    
    l1:
    for(var i in BOXES){
        
        l2:
        for(var j in exceptions){
            if(exceptions[j] == BOXES[i]){
                continue l1;
            }
        }
        
        BOXES[i].removeFocus();
    }
    var ind = exceptions.indexOf(FOCUS);
    if(ind == -1)
        FOCUS = null;
}

function updateWatchedStatus(){
    for(var i in BOXES){
        if(BOXES[i].type==0 && BOXES[i].watched == 1){
            for(var j in BOXES[i].outEdges){
                BOXES[i].outEdges[j].boxIn.setWatched(1);
            }
        }
    }
}

function updateContainerSize(){
   
    for(var i in BOXES){
        var cW = $CONTAINER.width();
        var cH = $CONTAINER.height();
        //console.log(BOXES[i].position.x + BOXES[i].$box.outerWidth()+ ' plus grand que '+ cW + '?');
        if(BOXES[i].position.x + BOXES[i].$box.outerWidth() > cW){
            
            $CONTAINER.css('width', BOXES[i].position.x + BOXES[i].$box.outerWidth());
        }
     
        if(BOXES[i].position.y + BOXES[i].$box.outerHeight() > cH){
            
            $CONTAINER.css('height', BOXES[i].position.y + BOXES[i].$box.outerHeight());
        }
    }
}

function getActiveParent(box, startBox){
    
    for(var i in box.inEdges){
        
        var parentBox = box.inEdges[i].boxOut;
        
        if(!parentBox.watched) continue;
        
        if(parentBox == startBox) return startBox;
        
        if(parentBox.focus)
            return getActiveParent(parentBox, startBox);
        
    }
    return box;
}

function getActiveChild(box){
    for(var i in box.outEdges){
        var childBox = box.outEdges[i].boxIn;
        if(childBox.watched && childBox.focus) return childBox;
        
    }
    return false;
}

function getFirstChild(box){
    if(box.outEdges.length > 0)
        return box.outEdges[0].boxIn;
    return false;
}

function getNextChild(box){
    var children = getActiveChildren(box);
    for(var i in children){
        if(!children[i].seen){
           children[i].seen = true; 
           return children[i];
        }
            
    }
    return false;
    
}
function getNextChildWithChild(box, debug){
    var children = getActiveChildren(box);
    for(var i in children){
        
        var grandChild = getFirstChild(children[i]);
        if(!grandChild) continue;
        
       
        if(!grandChild.seen){
           grandChild.seen = true; 
           return children[i];
        }
            
    }
    return false;
    
}
function getChildren(box){
    var children = [];
    for(var i in box.outEdges){
        var childBox = box.outEdges[i].boxIn;
        
        children.push(childBox);
    }
    return children;
}

function getActiveChildren(box){
    var children = [];
    for(var i in box.outEdges){
        var childBox = box.outEdges[i].boxIn;
        if(childBox.focus)
            children.push(childBox);
    }
    return children;
}

function getAllChildren(box, allChildren){
    
    if(allChildren == undefined)
        allChildren = [];
    
    var children = getChildren(box);
    
    if(children.indexOf(box) != -1)//we got a loop!
        return allChildren;
    
    for(var i in children){
        if(allChildren.indexOf(children[i]) != -1) continue;
        allChildren.push(children[i]);
        getAllChildren(children[i], allChildren);
    }
    
    
    
    return allChildren;
    
}

function resetChildrenDataPoint(box){
    var children = getAllChildren(box);
    
    for(var i in children){
        children[i].removeDataPoint();
    }
    return;
        
}

/*END MAIN FUNCTIONS DEFINITIONS*/

/*MAIN EVENTS DEFINITIONS*/

function onBoxDataPoint(){
    if(this.focus){
        //if active parent == current box -> data point
        var activeParent = getActiveParent(this, this);
        activeParent.setDataPoint();
        
        resetChildrenDataPoint(this);
        
        
        
        
        
    }else{
        this.removeDataPoint();
        var activeChildBox = false;
        for(var i in this.outEdges){
            childBox = this.outEdges[i].boxIn;
            activeChildBox = getActiveChild(childBox);
            if(activeChildBox != false)
                break;
                
        
        }
        console.log(childBox);
        if(activeChildBox != false){
            activeChildBox.setDataPoint();
        }
                           
    }
}

function onBoxDelete(){
    
    while(this.inEdges.length != 0){
       
       var ind = EDGES.indexOf(this.inEdges[0]);
       if (ind > -1) EDGES.splice(ind, 1);
       $(this.inEdges[0]).trigger('delete');
       
    }
   
    while(this.outEdges.length != 0){
        
        
        var ind = EDGES.indexOf(this.outEdges[0]);
        if (ind > -1) EDGES.splice(ind, 1);
        $(this.outEdges[0]).trigger('delete');
        
    }
    
    var ind = BOXES.indexOf(this);
    if (ind > -1) {
        BOXES.splice(ind, 1);
    }
    
    
    if(this.id != null){
        CHANGES.delete.push(this.id);
        ind = CHANGES.update.indexOf(this);
        if(ind > -1){
            CHANGES.update.splice(ind, 1);
            
        }
    }
    else{
        ind = CHANGES.insert.indexOf(this);
        if(ind > -1){
            CHANGES.insert.splice(ind, 1);
            
        }
    }
    
    this.destroy();
    FOCUS = null;
    
    
    
}


function onBoxFocusIn(){
    
    switch(MODE){
        case 0:
            console.log(FOCUS);
            if(FOCUS != null && FOCUS != this){
                
                if(FOCUS.activeConnector == 'in' && this.activeConnector == 'out'){
                    makeEdge(this, FOCUS);
                    removeFocus();
                }else if(FOCUS.activeConnector == 'out' && this.activeConnector == 'in'){
                    makeEdge(FOCUS, this);
                    removeFocus();
                }else{
                   FOCUS = this;
                   
                   removeFocus([this]);
                   
                }
                    
            }else{
               FOCUS = this; 
               removeFocus([this]);
               
            }
                
            if(CHANGES.update.indexOf(this) == -1 && this.id != null){
                
                CHANGES.update.push(this);
                
            }
                
            
            break;
        case 1:
            for(var i in this.outEdges){
                this.outEdges[i].boxIn.setFocus();
            }
            
    }
    

}
function onBoxFocusOut(){
    switch(MODE){
        case 1:
            if(this.type == 0){
                
                for(var i in this.outEdges){
                    this.outEdges[i].boxIn.removeFocus();
                }
                
            
            }
    }
        
}
function onBoxWatch(){
    if(this.type == 0)
        $(this).trigger('focusIn');
        
    if(this.watched == 1){
        for(var i in this.outEdges){
            if(this.outEdges[i].boxIn.watched == 0){
                this.outEdges[i].boxIn.setWatched(1);
                
                $(this.outEdges[i].boxIn).trigger('watch');
            }
        }
    }
    else{
       l1:
       for(var i in this.outEdges){
            if(this.outEdges[i].boxIn.watched == 1){
                //check if a parent is watched before unwatch
                l2:
                for(var j in this.outEdges[i].boxIn.inEdges){
                    if(this.outEdges[i].boxIn.inEdges[j].boxOut.watched == 1)
                        continue l1;
                }
                this.outEdges[i].boxIn.setWatched(0);
                
            }
        } 
    }
}



function onEdgeDelete(e){
    var ind = EDGES.indexOf(this);
    if (ind > -1) EDGES.splice(ind, 1);
    if(CHANGES.update.indexOf(this.boxOut) == -1 && this.boxOut.id != null){
        CHANGES.update.push(this.boxOut);
    }
    this.destroy(); 
    
}

function onBoxDrag(e){
    if(CTRL){//MEGADRAG
        var children = getAllChildren(this);
        
        for(var i in children){
            var move = {x:e.newPos.x - e.oldPos.x, y:e.newPos.y - e.oldPos.y};
            children[i].move(move);
            if(CHANGES.update.indexOf(children[i]) == -1 && children[i].id != null){
                CHANGES.update.push(children[i]);
                
            }
        }
        
    }
}

/*END MAIN EVENTS DEFINITIONS*/

var Edge = function(boxOut, boxIn, container){
    this.boxIn = boxIn;
    this.boxOut = boxOut;
    this.$container = $(container);
    
    this.createEdge = function(){
        this.$edge = $('<div class="edge">');
        this.$container.append(this.$edge);
        this.renderEdge();
    }
    this.renderEdge = function(){
        var upConnectorH = -5;
        var downConnectorH = 0;
        var x1 = this.boxIn.$box.position().left + this.boxIn.$box.outerWidth()/2;
        var y1 = this.boxIn.$box.position().top-upConnectorH;
        var x2 = this.boxOut.$box.position().left + this.boxIn.$box.outerWidth()/2;
        var y2 = this.boxOut.$box.position().top + this.boxOut.$box.outerHeight()+downConnectorH;
        
        var edgeWidth =  Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
        var angle = Math.atan2((y1-y2), (x1-x2)) *  (180/Math.PI);
        
        this.$edge.css({'top':y2, 'left':x2, 'width':edgeWidth, 'transform' : 'rotate('+ angle +'deg)'});
       
        
        
    }
    this.initEvents = function(){
        var _this = this;
        if(MODE == 1) return;
        
        this.$edge.on('mouseover', function(){
            $(this).addClass('active');
        });
        this.$edge.on('mouseout', function(){
            $(this).removeClass('active');
        });
        this.$edge.on('click', function(e){
            e.stopPropagation();
            $(_this).trigger('delete');
            
        });
    
    }
    
    this.hide = function(){
        this.$edge.hide();
    }
    this.show = function(){
        this.$edge.show(); 
    }
    
    this.destroy = function(){
        this.boxOut.removeOutEdge(this);
        this.boxIn.removeInEdge(this);
        this.$edge.remove();
    }
    
    
    this.createEdge();
    

}

var Box = function(container){
    
    this.outEdges = [];
    this.inEdges = [];
    this.$container = $(container);
    this.activeConnector = null;
    this.id = null;
    this.inId = new Date().getUTCMilliseconds();
    this.focus = false;
    this.dataPoint = false;
    this.textareaMode = false;
    
    this.createBox = function(){
        
        this.$box = $('<div class="box" unselectable="on">' +
                          '<div class="connector ui" data-direction="in"></div>' +
                          '<div class="watch ui" data-status="0" data-mode="0"></div>' +
                          '<div class="freq" data-mode="1"></div>' +
                          '<div class="inside">' +
                              '<div class="box-type ui" data-type="0"></div>' +
                              '<div class="admin-url ui" data-mode="0"></div>'+
                              '<div class="del ui" data-mode="0"></div>' +
                              '<div class="content" unselectable="on"></div>' +
                          '</div>' +
                          '<div class="connector ui" data-direction="out"></div>' +
                        '</div>'
                    );
        this.$content = this.$box.find('.content');
        this.$uiDel = this.$box.find('.del');
        this.$connectors = this.$box.find('.connector');
        this.$uiType = this.$box.find('.box-type');
        this.$uiWatched = this.$box.find('.watch');
        this.$freq = this.$box.find('.freq');
        this.$container.append(this.$box);
        //console.log(this.$box.outerHeight()/2);
        this.mouseOffset = {'x':this.$box.outerWidth()/2, 'y':this.$box.outerHeight()/2};
        this.type = this.$uiType.data('type');
        this.watched = this.$uiWatched.data('status');
        
    };
    
    this.initEvents = function(){
        var _this = this;
        
        
        
        if(MODE == 1) {
             this.$box.on('click', function(e){
                e.stopPropagation();
                
                if(_this.type == 0){
                    _this.switchFocus();
                    $(_this).trigger('dataPoint');
                }
            
            });
            return;
        }
        
        this.$box.on('click', function(e){
            e.stopPropagation();
        });
        
        this.$box.on('dblclick', function(e){
            e.stopPropagation();
            _this.setFocus();
            _this.textareaModeOn();
        });
        this.$box.on('mousedown', function(e){
            e.stopPropagation();
            _this.switchFocus();
            if(_this.focus){
                _this.draggable = true;
                _this.mouseOffset = {'x':e.pageX - $(this).position().left, 'y':e.pageY - $(this).position().top};
            }
        });
        this.$box.on('mouseup', function(e){
            e.stopPropagation();
            _this.draggable = false;
        });
        this.$box.on('mousemove', function(e){
            e.stopPropagation();
            if(_this.draggable)
                _this.setPosition(e.pageX - _this.mouseOffset.x, e.pageY - _this.mouseOffset.y);
        });
        
        this.$box.on('mouseout', function(e){
            
            if(_this.draggable)
                _this.setPosition(e.pageX - _this.mouseOffset.x, e.pageY - _this.mouseOffset.y);
        });
        
        
        this.$connectors.on('mousedown', function(e){
            e.stopPropagation();
            
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                _this.activeConnector = null;
            }else{
               _this.$connectors.removeClass('active');
               $(this).addClass('active');
                _this.activeConnector = $(this).data('direction'); 
            }
            _this.setFocus();
        });
        
        
        this.$uiDel.on('mousedown', function(e){
            e.stopPropagation();
            
            $(_this).trigger('delete');
        });
        
        this.$uiType.on('mousedown', function(e){
            e.stopPropagation();
            if(_this.inEdges.length == 0 && _this.outEdges.length == 0){
                _this.switchType();
            }
        });
        this.$uiWatched.on('mousedown', function(e){
            e.stopPropagation();
            if(_this.type == 0){
                _this.switchWatch();
                $(_this).trigger('watch');
            }
            
        });
        
        
    };
    
    this.getDBObj = function(){
        var dbObj = {};
        dbObj.id = this.id;
        dbObj.inId = this.inId;
        dbObj.position = this.position;
        dbObj.type = this.type;
        dbObj.watched = this.watched;
        dbObj.title = this.$content.text();
        dbObj.out = [];
        for(var i in this.outEdges){
            dbObj.out.push({'inId':this.outEdges[i].boxIn.inId, 'id':this.outEdges[i].boxIn.id});
        }
        return dbObj;
        
    }
    
    this.switchType = function(){
        this.type = (this.type == 0)?1:0;
        this.$uiType.attr('data-type', this.type);
    }
    
    this.switchWatch = function(){
        this.watched = (this.watched == 0)?1:0;
        this.$uiWatched.attr('data-status', this.watched);
    }
    
    this.switchFocus = function(){
        if(this.focus){
            this.removeFocus();
            
        }else{
            this.setFocus();
        }
    }
    
    this.setFocus = function(){
        if(this.focus) return false;
        
        this.focus = true;
        this.$box.addClass('active');
        $(this).trigger('focusIn');
        
    }
    
    this.removeFocus = function(){
        this.focus = false;
        this.textareaModeOff();
        this.$box.removeClass('active');
        this.activeConnector = null;
        this.$connectors.removeClass('active');
        $(this).trigger('focusOut');
    }
    this.setPosition = function(x, y){

        x = (x < 0)?0 : x;
        y = (y < 0)?0 : y;
        
        var newPos = {'x':x, 'y':y};
        if(this.draggable)
            $(this).trigger({type:'drag', oldPos:this.position, newPos:newPos});
        
        this.$box.css({'top':y, 'left':x});
        this.position = newPos;
        
        this.renderEdges();
        
    }
    
    this.move = function(movement){
        this.setPosition(this.position.x + movement.x, this.position.y + movement.y);
    }
    
    this.renderEdges = function(){
        for(var i in this.inEdges){
            this.inEdges[i].renderEdge();
        }
        for(var i in this.outEdges){
            this.outEdges[i].renderEdge();
        }
        
    }
    
    this.setFreq = function(freq){
        this.$freq.text(freq);
    }
    
    this.setDataPoint = function(){
        if(this.dataPoint) return;
        
        this.dataPoint = true;
        
        for(var i in this.$connectors){
            if($(this.$connectors[i]).data('direction') == 'in'){
                var $inConnector = $(this.$connectors[i]);
                break;
            }
        }
        
        
        $inConnector.attr('data-direction', 'start');
        $inConnector.data('direction', 'start');
        
        
    }
    this.removeDataPoint = function(){
        if(!this.dataPoint) return;
        
        this.dataPoint = false;
        
        for(var i in this.$connectors){
            if($(this.$connectors[i]).data('direction') == 'start'){
                var $inConnector = $(this.$connectors[i]);
                break;
            }
        }
        
        
        $inConnector.attr('data-direction', 'in');
        $inConnector.data('direction', 'in');
        
        
    }
    this.setType = function(type){
        this.type = type;
        this.$uiType.attr('data-type', this.type);
    }
    
    this.setWatched = function(status){
        this.watched = status;
        this.$uiWatched.attr('data-status', this.watched);
    }
    
    this.setAdminURL = function(url){
        this.$box.find('.admin-url.ui').html('<a target="_blank" href="'+url+'">edit</a>');
    }
    
    this.setText = function(text){
        this.$content.text(text);
        this.renderEdges();
    }
    
    this.addChar = function(c){
        this.$content.text(this.$content.text() + c);
        this.renderEdges();
    }
    this.removeChar = function(){
        this.$content.text(this.$content.text().slice(0, -1));
        this.renderEdges();
    }
    
    this.addOutEdge = function(edge){
        this.outEdges.push(edge);
        
    }
    this.addInEdge = function(edge){
        this.inEdges.push(edge);
        
    }
    
    this.removeOutEdge = function(edge){
        var ind = this.outEdges.indexOf(edge);
        if (ind > -1) {
            this.outEdges.splice(ind, 1);
        }
    }
    this.removeInEdge = function(edge){
        var ind = this.inEdges.indexOf(edge);
        if (ind > -1) {
            this.inEdges.splice(ind, 1);
        }
    }
    this.destroy = function(){
        this.$box.remove();
        
    }
    this.hide = function(){
        this.$box.hide();
        for(var i in this.inEdges){
            this.inEdges[i].hide();
        }
        for(var i in this.outEdges){
            this.outEdges[i].hide();
        }
    }
    this.show = function(){
        this.$box.show();
        for(var i in this.inEdges){
            this.inEdges[i].show();
        }
        for(var i in this.outEdges){
            this.outEdges[i].show();
        }
    }
    this.textareaModeOn = function(){
        if(this.textareaMode) return;
        
        this.textareaMode = true;
        var content = this.$content.text();
        var currDim = {'w':this.$content.width(), 'h':this.$content.height()};
        this.$content.css({'width':currDim.w, 'height':currDim.h});
        this.$content.empty();
        var $textarea = $('<textarea>'+content+'</textarea>');
        $textarea.on('mousedown', function(e){
            e.stopPropagation();
        }).on('keydown', function(e){
            e.stopPropagation();
        }).on('keypress', function(e){
            e.stopPropagation();
        });
        this.$content.append($textarea);
        
    }
    this.textareaModeOff = function(){
        if(!this.textareaMode) return;
        
        this.textareaMode = false;
        var textarea = this.$content.find('textarea');
        if(textarea.length > 0)
            var content = this.$content.find('textarea').val();
        else
            var content = this.$content.text();
        
        this.$content.css({'width':'auto', 'height':'auto'});
        this.$content.empty();
        
        this.setText(content);
        
    }
    
    this.createBox();

}

var Analyser = function(dataPoints, container){
    this.dataPoints = dataPoints;
    this.$container = $(container);
    
    this.init = function(){
        
        for(var i in dataPoints){
            var $section = $('<section class="trails">');
            this.$container.append($section);
            
            var currentTrail = [];
            var trails = [];
            
            this.renderTrails(dataPoints[i], trails, currentTrail, $section);
            
            //this.renderTrails(dataPoints[i], currentTrail, $section);
            
            //var children = getActiveChildren(dataPoints[i]);
            //var sibblings = [];
            //this.renderTrails2(dataPoints[i], children, 0, sibblings, 0, currentTrail, $section);
            //trails = this.makeTrails(dataPoints[i], trails, currentTrail);
            
            //this.showTrail(trails, $section, 0);
            
        }
        
    }
    
    this.getDBTrail = function(trail){
        var output = '';
        for(var i in trail){
            var box = trail[i];
            if(i > 0 && box.type == 0) output += '|';
            else if(box.type == 1) output += '>';
            output += box.id;
        }
        return output;
    }
    
    this.renderTrails = function(dataPoint, trails, parentTrail, $section){
        //console.log(parentTrail);
        var ind = parentTrail.indexOf(dataPoint); 
        var _this = this;
        if(ind > -1){//oops we got a loop!
            return trails;
        }
        
        //if datapoint = question : get next answer and render
        if(parentTrail.length % 2 == 0){
            
            var nextChild = getNextChild(dataPoint); 
            if(nextChild != false){
                parentTrail.push(dataPoint);
                this.renderTrails(nextChild, trails, parentTrail, $section);
            }else{
                //on tente de choper la sous-question suivante
                var nextChild = getNextChildWithChild(dataPoint);
                if(nextChild != false){
                    var nextGrandchild = getFirstChild(nextChild);
                    parentTrail.push(dataPoint);
                    parentTrail.push(nextChild);
                    _this.renderTrails(nextGrandchild, trails, parentTrail, $section);
                }else{
                    //on va remonter le parentTrail à la recherche d'une question dont une sous-question n'a pas été traitée
                    for(var i = parentTrail.length - 2; i >= 0; i-=2){
                        alert('à la question '+parentTrail[i].$content.text());
                        var nextChild = getNextChildWithChild(parentTrail[i], true);
                        
                        if(nextChild != false){
                            alert('wowowwww une nouvelle réponse: '+nextChild.$content.text());
                            var nextGrandchild = getFirstChild(nextChild);
                            parentTrail = parentTrail.slice(0, i);
                            parentTrail.push(nextChild);
                            _this.renderTrails(nextGrandchild, trails, parentTrail, $section);
                            break;  
                        }
                    }
                }
            
            }
        }else{
            var question = parentTrail[parentTrail.length - 1];
            var answer = dataPoint;
            var currentTrail = parentTrail.slice();
            currentTrail.push(answer);
            trails.push(currentTrail);
            this.showTrail(currentTrail, $section, function(){
                
                var oldParentTrail = parentTrail.slice(0, -1);
                //on relance pour afficher les autres chemins de la même question
                _this.renderTrails(question, trails, oldParentTrail, $section);
            
                
            });
            
            
            
        
        }
        /*
        for(var i in dataPoint.outEdges){
            var currentTrail = parentTrail.slice();
            var answer = dataPoint.outEdges[i].boxIn;
            
            
            currentTrail.push(dataPoint);
            currentTrail.push(answer);
            trails.push(currentTrail);
            
            if(trails.length > 10){
                console.log(trails);
                /*this.showTrail(trails, $section, 0, function(){
                    //trails = [];
                    for(var j in answer.outEdges){
                        if(answer.outEdges[j].boxIn.focus)
                            _this.renderTrails(answer.outEdges[j].boxIn, trails, currentTrail, $section);
                    }
                });
            }else{
                for(var j in answer.outEdges){
                    if(answer.outEdges[j].boxIn.focus)
                        this.renderTrails(answer.outEdges[j].boxIn, trails, currentTrail, $section);
                }
            }
                
            
        }*/
        
    }
    
    this.showTrail = function(trail, $section, callback){
        var _this = this;
        
        var $trail = $('<div class="trail">');
        $section.append($trail);
        var DBTrail = this.getDBTrail(trail);
        $.ajax({
            url : $(location).attr('href'),
            type: "POST",
            data : {'action':'getTrailCount', 'trail':DBTrail},
           
            dataType:'json',
            success:function(data, textStatus, jqXHR){
                
                for(var k in trail){
                    var box = trail[k];
                    $trail.append(box.$box.clone());
                }
                
                $trail.append('<div class="count">'+data.count+'</div>');
                if(typeof callback == "function") callback();
                
                
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(errorThrown);
            }
        });
    }
    this.showTrails = function(trails, $section, ind, callback){
        var _this = this;
        var trail = trails[ind];
        var $trail = $('<div class="trail">');
        $section.append($trail);
        var DBTrail = this.getDBTrail(trail);
        
        $.ajax({
            url : $(location).attr('href'),
            type: "POST",
            data : {'action':'getTrailCount', 'trail':DBTrail},
           
            dataType:'json',
            success:function(data, textStatus, jqXHR){
                
                for(var k in trail){
                    var box = trail[k];
                    $trail.append(box.$box.clone());
                }
                
                $trail.append('<div class="count">'+data.count+'</div>');
                
                if(ind < trails.length - 1)
                    _this.showTrail(trails, $section, ind + 1, callback);
                else if(typeof callback == "function") callback()
                
                
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(errorThrown);
            }
        });
        
        
        
    }
    
    this.init();
    
    
}


loadStructure();
initEvents();

