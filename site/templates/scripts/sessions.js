var pathI = 0;
var pathsNb = 10;
var params = {action:'export', start:pathI, limit:pathsNb};

var url = new URL(location.href);
//url.search = new URLSearchParams(params);

function log(msg){
    document.body.querySelector('section.log').innerHTML += '<p>'+msg+'</p>';
}


function deleteall(){
    log('deleting all the sessions');
    var params = {action:'delete'};
    fetch(url, {
            credentials: "same-origin",
            headers:{
                'X-Requested-With':'XMLHttpRequest',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(params)
        }
    )
    .then(
        response => response.json()
    )
    .then(
        data => {
            log(data.msg);
        }
    )
    .catch(
        error => console.log(error.message)
    );

}

function buildcsv(launchfill = false){
    log('building csv header');
    var params = {action:'buildcsv'};
    fetch(url, {
            credentials: "same-origin",
            headers:{
                'X-Requested-With':'XMLHttpRequest',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(params)
        }
    )
    .then(
        response => response.json()
    )
    .then(
        data => {
            log(data.msg);
            if(launchfill){
                fillcsv();
            }
        }
    )
    .catch(
        error => console.log(error.message)
    );



}

function fillcsv(pathI = 0, pathNb = 10){
    log('loading paths from '+pathI+' to '+(pathI+pathNb));
    var params = {action:'fillcsv', start:pathI, limit:pathNb};
    fetch(url, {
        credentials:'same-origin',
        headers:{
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(params)
    }).then(response => response.json()
    ).then(data => {
        if(data.msg == 'next'){
            fillcsv(pathI+pathNb);
        }else{
            log('Import is done. <a href="/site/templates/export.csv">Take your file</a> and goodbye.')
        }
    }
    ).catch(
        error => console.log(error.message)
    );




}

function start(){
    buildcsv(true);
}

const $actionElements = document.body.querySelectorAll('section.actions p');
[...$actionElements].forEach($actionElement => {
    $actionElement.addEventListener('click', evt => {
        console.log(evt.target.dataset.action);
        return(eval(evt.target.dataset.action+'()'));

    });
});


/*
fetch(url, {
        credentials: "same-origin",
        headers:{
            'X-Requested-With':'XMLHttpRequest',
            'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(params)
    }
)
.then(
    response => response.json()
)
.then(
    data => {
        console.log(data)
    }
)
.catch(
    error => console.log(error.message)
);
*/
