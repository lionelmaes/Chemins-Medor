<?php
//DATA
    $q = $parentQuestion->id;
    $a = $page->id;

    if(!$session->get('id') || !$pages->get($session->get('id'))->id){

            $p = new Page();
            $p->setOutputFormatting(false);
            $p->template = 'session';

            $p->parent = $page->parent('template=chemins')->child('template=sessions');

            $p->save();

            $session->set('id', $p->id);
    }
    else{
        $p = $pages->get($session->get('id'));
        $p->setOutputFormatting(false);
    }

    $currentTrail = $p->trail;

    $step = $q.'>';

        if(isset($input->post->data)){
            //check for data here
            $input->post->data = $sanitizer->textarea($input->post->data);

            if(!preg_match('#[a-z0-9]+#i', $input->post->data)){
                echo 'data_error_empty';
                exit();
            }
            if($page->textfield_type->id == 1){//number
                if(!preg_match('#^[0-9]+$#', $input->post->data)){
                    echo 'data_error_type_number';
                    exit();
                }
            }
            if(isset($a->textfield_nbchars) && $a->textfield_nbchars > 0){
                if(strlen($input->post->data) > $a->textfield_nbchars){
                    echo 'data_error_nbchars';
                    exit();
                }
            }

            if($page->textfield_sendemail){
                $subject = $config->mailSubject;
                $body = 'Question: '.$sanitizer->unentities($parentQuestion->title)."\n".'Answer: '.$sanitizer->unentities($page->title)."\n".'Data: '.$input->post->data;
                $mail->send($page->textfield_sendemail, $config->mailAddress, $subject, $body);
            }
            if(strpos($currentTrail, $step) === false){//question not answered already
                $data = $p->textdatas->getNew();
                $data->question = $q;
                $data->answer = $a;
                $data->textdata = $input->post->data;
                $data->save();
            }

        }
        if(strpos($currentTrail, $step) === false || !$config->recordOnce){//question not answered already
            $p->trail = $currentTrail.(($currentTrail)?'|':'').$q.'>'.$a;

            $p->save();
        }




    //
?>
