<?php
$question = $page->child('template=questions')->child('home=1');

if($question->id)
    include('question.php');
else
    echo "No question with \"home checkbox\" found";
?>
