<?php

    $parentQuestion = $pages->get('template=question, link_answer='.$page->id);

    if($parentQuestion->watched == 1){

        include('inc/data.inc');
    }

?>

<?php

    if(empty($page->link_question))
        header('Location: /index.php');

    $question = $page->link_question;
    include('question.php');

?>
</div>
