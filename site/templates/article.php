<?php

    if(!$config->ajax){
        include("./inc/header.inc");
    }

    $article = $page;
?>
<?php

    if($article->image):
?>

<section class="illu">
        <img src="<?php echo $article->image; ?>" />
</section>

<?php
    endif;
?>

    <section class="page">
    <section class="article-full>">
        <div class="article-container">
          <h3><?php echo $article->title; ?></h3>
          <div class="detail"><?php echo $article->text; ?></div>
        </div>
    </section>
</section>


<?php
    if(!$config->ajax){
        include("./inc/footer.inc");
    }
?>
