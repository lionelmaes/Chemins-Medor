<?php
    if(!isset($question) && isset($_POST['name'])){
        $question = $pages->find('name='.$_POST['name']);
    }
    
    
    $answers = $question->link_answer;
    
?>

<section class="page">
    <section class="question">
        <h2><?php echo $question->title; ?></h2>
        <div class="detail"><?php echo $question->text; ?></div>
    </section>
<?php
    if(count($answers) > 0):
?>
    <section class="answers">
<?php
        foreach($answers as $answer):
            $question = $answer->link_question;
           
?>

        <div class="answer">
<?php
            if($question):
                
?>
            <a class="navig-link" href="<?php echo $question->name; ?>">
<?php
            endif;
?>
            <h3><?php echo $answer->title; ?></h3>
            <div class="detail"><?php echo $answer->text; ?></div>
<?php
            if($question):
?>
            </a>
<?php
            endif;
?>
        </div>

<?php
        endforeach;
?>
        
        
        
        
    </section>
<?php
    endif;
?>
    
</section>
